
let boardState = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
let currentPlayer = 0, playerWins = 0, pcWins = 0, tie = 0;
let gameFinished = false;
let currentMove = 1;

function getCellId(r, c) {
  return `c${r+1}${c+1}`;
}

function markCell(r, c) {
  if(gameFinished || !!boardState[r][c])
    return;

  if(currentPlayer === 0) {
    boardState[r][c] = 1;
    document.getElementById(getCellId(r, c)).getElementsByClassName('x')[0].style.visibility = 'visible';
  } else {
    boardState[r][c] = 2;
    document.getElementById(getCellId(r, c)).getElementsByClassName('o')[0].style.visibility = 'visible';
  }

  currentPlayer ^= 1;

  currentMove++;

  document.getElementById('move').innerText = 'Current Move: ' + currentMove;
  document.getElementById('turn').innerText = currentPlayer===0 ? 'Your Turn' : 'PC Turn';

  checkIfGameIsFinished( true);

  if(!gameFinished && currentPlayer === 1)
    pcTurn();
}

function calcPCMove(state, player) {
  let R = [[0, 0, 0], [0, 0, 0]], C = [[0, 0, 0], [0, 0, 0]];
  let d = [0, 0], id = [0, 0];
  for(let i=0; i<3; i++) {
    for(let j=0; j<3; j++) {
      if(boardState[i][j] !== 0) {
        R[boardState[i][j]-1][i]++;
        C[boardState[i][j]-1][j]++;
        if(i===j) d[boardState[i][j]-1]++;
        if(i===2-j) id[boardState[i][j]-1]++;
      }
    }
  }
  //check if pc can win
  for(let i=0; i<3; i++) {
    if(R[1][i]===2) {
      for(let j=0; j<3; j++) {
        if(boardState[i][j]===0)
          return {r: i, c: j};
      }
    }
    if(C[1][i]===2) {
      for(let j=0; j<3; j++) {
        if(boardState[j][i]===0)
          return {r: j, c: i};
      }
    }
    if(d[1]===2 && boardState[i][i]===0)
      return {r: i, c: i};
    if(id[1]===2 && boardState[i][2-i]===0)
      return {r: i, c: 2-i};
  }
  //check if pc can prevent lose
  let best = 0, atR = -1, atC = -1;
  for(let i=0; i<3; i++) {
    for(let j=0; j<3; j++) {
      if(boardState[i][j]!==0) continue;
      let cnt = 0;
      if(R[0][i]===2) cnt++;
      if(C[0][j]===2) cnt++;
      if(i===j && d[0]===2) cnt++;
      if(i===2-j && id[0]===2) cnt++;
      if(cnt > best) {
        best = cnt;
        atR = i;
        atC = j;
      }
    }
  }
  if(atR !== -1)
    return {r: atR, c: atC};
  for(let i=0; i<3; i++) {
    if(R[0][i]===2) {
      for(let j=0; j<3; j++) {
        if(boardState[i][j]===0)
          return {r: i, c: j};
      }
    }
    if(C[0][i]===2) {
      for(let j=0; j<3; j++) {
        if(boardState[j][i]===0)
          return {r: j, c: i};
      }
    }
    if(d[0]===2 && boardState[i][i]===0)
      return {r: i, c: i};
    if(id[0]===2 && boardState[i][2-i]===0)
      return {r: i, c: 2-i};
  }
  //try to get best move
  for(let i=0; i<3; i++) {
    for(let j=0; j<3; j++) {
      if(boardState[i][j]!==0) continue;
      if(R[1][i]>0 || C[1][j]>0 || (i===j && d[1]>0) || (i===2-j && id[1]>0)) {
        return {r: i, c: j};
      }
      if(i===1 && j===1)
        return {r: i, c: j};
    }
  }
  for(let i=0; i<3; i++) {
    for(let j=0; j<3; j++) {
      if(boardState[i][j]!==0) continue;
      if(i===1 || j===1)
        return {r: i, c: j};
    }
  }
  //get any available move
  for(let i=0; i<3; i++)
    for(let j=0; j<3; j++)
      if(boardState[i][j]===0)
        return {r: i, c: j};

  return undefined;
 }

function getPCMove() {
  let state = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
  for(let i=0; i<3; i++)
    for(let j=0; j<3; j++)
      state[i][j] = boardState[i][j];

  for(let i=0; i<3; i++) {
    for(let j=0; j<3; j++) {
      if(state[i][j] !== 0) continue;
      state[i][j] = 2;
      let res = calcPCMove(state, 2);
      if(res) {
        console.log(`PC Move: ${res.r}, ${res.c}`);
        return res;
      }
      state[i][j] = 0;
    }
  }
  return undefined;
}

function pcTurn() {
  let o = getPCMove();
  setTimeout(() =>
    markCell(o.r, o.c),
    500);
}

function updateWinCounts(winner) {
  if(winner===1) {
    playerWins++;
    document.getElementById('player-cnt').innerText = 'Player (X):   ' + playerWins;
    setTimeout(function (){window.alert('You won the game!')});
  }
  else if(winner===2) {
    pcWins++;
    document.getElementById('pc-cnt').innerText = 'PC (O):   ' + pcWins;
    setTimeout(() => window.alert('You lose the game!'));
  } else {
    tie++;
    document.getElementById('tie-cnt').innerText = 'TIE:   ' + tie;
    setTimeout(() => window.alert('You tied with PC in this game!'));
  }
}

function getGameWinner() {
  let R = [[0, 0, 0], [0, 0, 0]], C = [[0, 0, 0], [0, 0, 0]];
  let d = [0, 0], id = [0, 0];
  for(let i=0; i<3; i++) {
    for(let j=0; j<3; j++) {
      if(boardState[i][j] !== 0) {
        R[boardState[i][j]-1][i]++;
        C[boardState[i][j]-1][j]++;
        if(i===j) d[boardState[i][j]-1]++;
        if(i===2-j) id[boardState[i][j]-1]++;
      }
    }
  }
  for(let p=1; p<=2; p++) {
    if(R[p-1][0]===3 || R[p-1][1]===3 || R[p-1][2]===3 ||
       C[p-1][0]===3 || C[p-1][1]===3 || C[p-1][2]===3 ||
       d[p-1]===3 || id[p-1]===3)
      return p;
  }
  return 0;
}

function checkIfGameIsFinished(doUpdate) {
  let winner = getGameWinner();
  if(winner !== 0) {
    if(doUpdate) {
      gameFinished = true;
      updateWinCounts(winner);
    }
    return winner;
  }
  let total = 0;
  for(let i=0; i<3; i++)
    for(let j=0; j<3; j++)
      if(boardState[i][j] !== 0)
        total++;
  if(total === 9) {
    if(doUpdate) {
      gameFinished = true;
      updateWinCounts(-1);
    }
    return 0;
  }
  return -1;
}

document.getElementById("new-game-btn").addEventListener("click", (ev) => {
  boardState = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
  currentPlayer = 0;
  gameFinished = false;
  currentMove = 1;

  setTimeout(function () {
    ['x', 'o']
      .forEach((s) => {
        for(let i=0; i<3; i++)
          for(let j=0; j<3; j++) {
            document.getElementById(getCellId(i,j)).getElementsByClassName(s)[0].style.visibility = 'hidden';
          }
      });
    document.getElementById('turn').innerText = 'Your Turn';
    document.getElementById('move').innerText = 'Current Move: ' + currentMove;
  });
});

for(let i=0; i<3; i++) {
  for (let j = 0; j < 3; j++) {
    document.getElementById(getCellId(i, j)).addEventListener("click",
      (ev) => markCell(i, j));
  }
}
